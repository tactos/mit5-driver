#include "device.h"
#include<iostream>


/**
 * @brief look on system configuration for 2 unuesed keycode to be used by Mit5 buttons
 * 
 * @details this function look for the first and the last unused keycode
 * 
 * @return std::pair< uint8_t, uint8_t >
 */
std::pair<uint8_t, uint8_t> get_unused_keycode() {
    std::array<char, 128> buffer;
    std::string result;
    std::unique_ptr<FILE, decltype(&pclose)> first_pipe(popen("xmodmap -pke | grep -E \"[0-9]{3} =$\" | cut -d \" \" -f 2 | tail -n 1", "r"), pclose);
    if (!first_pipe) {
        throw std::runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), first_pipe.get()) != nullptr) {
        result += buffer.data();
    }
    uint8_t first_keycode = std::stoul(result);
    result.clear();
    
    std::unique_ptr<FILE, decltype(&pclose)> last_pipe(popen("xmodmap -pke | grep -E \"[0-9]{3} =$\" | cut -d \" \" -f 2 | head -n 1", "r"), pclose);
    if (!last_pipe) {
        throw std::runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), last_pipe.get()) != nullptr) {
        result += buffer.data();
    }
    uint8_t last_keycode = std::stoul(result);
    std::cout << "found key codes " << std::to_string(first_keycode) << " and " << std::to_string(last_keycode) << std::endl;
    return {first_keycode, last_keycode};
}



int main()
{
    auto keys = get_unused_keycode();
    Mit5::Device mit5(keys);
    mit5.start();

}
