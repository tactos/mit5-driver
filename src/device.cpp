#include "device.h"
#include <thread>
#include <X11/extensions/XTest.h>

using namespace Mit5;


constexpr uint8_t READ_DELAY = 100;
constexpr uint8_t WRITE_DELAY = 100;

/**
 * @brief MIT5 device representation
 * 
 * @param button_keycodes device use fake key events for button press. Choose 2 keycode used
 * @param address location in /dev/…
 * @param baud_rate serial speed communication
 * @param char_size serial Character Size
 * @param flow serial flow control
 * @param parity serial parity parameter
 * @param stopbits serial stopbits parameter
 */
Device::Device(std::pair<uint8_t, uint8_t> button_keycodes, std::string address, 
               LibSerial::BaudRate baud_rate, LibSerial::CharacterSize char_size, 
               LibSerial::FlowControl flow, LibSerial::Parity parity, LibSerial::StopBits stopbits)
: matrix(0), button_1(0), button_2(0),
button_1_code(button_keycodes.first), button_2_code(button_keycodes.second)
{
    this->port.Open(address);
    this->port.SetBaudRate(baud_rate);
    this->port.SetCharacterSize(char_size);
    this->port.SetFlowControl(flow);
    this->port.SetParity(parity);
    this->port.SetStopBits(stopbits);
    
    
    // set buttons to initial value.
    while (!port.IsDataAvailable()) { } // wait until data available to set button initial value
    get_buttons_status();
    
    // initialize device in known state
    write_trame(0x00, 0xff); 
}


/**
 * @brief compute crc16 of trame to send
 * 
 * @param trame value to compute crc16
 * @return crc16 computed
 */
std::pair< uint8_t, uint8_t > Mit5::Device::crc16(const std::array< unsigned char, 4 > trame)
{
    uint16_t crc = 0x0000;
    
    for (u_char byte : trame)
    {
        crc = (crc >> 8) ^ Device::crc_tab16[ (crc ^ (uint16_t) byte) & 0x00FF ];
    }
    
    
    return {(crc  & 0xff), (crc >> 8)};
}

/**
 * @brief thread in charge of reading date send by MIT5 and send fake key event if button are pressed
 * 
 */
void Mit5::Device::read_thread()
{
    this->display= XOpenDisplay(0); //open connexion with X server
    if ( this->display == 0 ) {
        throw std::runtime_error ( "unable to open default control connection" );
    }
    while (true)
    {
        auto buttons_status  = get_buttons_status();
        if (buttons_status.first)
        {
            XTestFakeKeyEvent(display, button_1_code, True, 0);
            XTestFakeKeyEvent(display, button_1_code, False, 0);
            XFlush(display);
            std::cout << "first button pressed" << std::endl;
        }
        if (buttons_status.second)
        {
            XTestFakeKeyEvent(display, button_2_code, True, 0);
            XTestFakeKeyEvent(display, button_2_code, False, 0);
            XFlush(display);
            std::cout << "second button pressed" << std::endl;
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(READ_DELAY));
    }
}

/**
 * @brief extract button status from data.It execute every READ_DELAY ms.
 * 
 * @details get all data available on serial port and read  last trame. button values are counter increased
 * each time a button is pressed.
 * 
 * @return std::pair< bool, bool >
 */
std::pair<bool, bool> Mit5::Device::get_buttons_status()
{
    std::string data;
    {
        std::scoped_lock lock(this->serialPortMut);
        if(this->port.IsDataAvailable()) 
        {
            while (this->port.IsDataAvailable()) // get the last trame emited
            {
                this->port.Read(data, 12); // mit5 trame is size 12
            }
        } else {
            return { false, false};}
    }
    
    auto button_1_status = (data[8] != button_1);
    auto button_2_status = (data[7] != button_2);
    button_2 = data[7];
    button_1 = data[8];
    
    return { button_1_status, button_2_status};
    
}

/**
 * @brief thread in charge of writing data from local  buffer to MIT5 it execute every WRITE_DELAY ms
 * 
 */
void Mit5::Device::write_thread()
{
    std::cout << "launch write thread" << std::endl;
    while (true)
    {
        uint8_t cell1 = 0;
        uint8_t cell2 = 0;
        {
            std::unique_lock lock(this->matrixMut);
            cell1 = matrix & 0xff;
            cell2 = matrix >> 8;
            matrix = 0;
            dbus_updated = false;
            this->dbus_write.wait(lock);
        }
        write_trame(cell1, cell2);
        std::this_thread::sleep_for(std::chrono::milliseconds(WRITE_DELAY));
        
    }
}

/**
 * @brief compute checksum and send data through serial port
 * 
 * @param cell1 
 * @param cell2 
 */
void Mit5::Device::write_trame(uint8_t cell1, uint8_t cell2)
{
    auto chksum = crc16({ 0x50, 0x2C, cell1, cell2}) ;
    std::vector<uint8_t> out_buffer  {0x24, 0x50, 0x2C, cell1, cell2, 0x2a, chksum.first, chksum.second};
    port.Write(out_buffer);
}



/**
 * @brief thread listening dbus interface
 * 
 */
void Mit5::Device::dbus_thread()
{
    Glib::init();
    Gio::init();
    
    proxy = org::tactos::ipcProxy::createForBus_sync(Gio::DBus::BUS_TYPE_SESSION,
                                                             Gio::DBus::PROXY_FLAGS_NONE,
                                                             "org.tactos.ipc",
                                                             "/org/tactos/ipc",
                                                             Gio::Cancellable::create());
    
   proxy->Matrix_changed().connect(std::bind(&Device::on_matrix_updated, this));
    proxy->readerJoin_sync();
    
    Glib::RefPtr<Glib::MainLoop> ml = Glib::MainLoop::create();
    ml->run();
}

/**
 * @brief perform an OR on the current buffered value and the new arriving one. 
 * 
 * @details if the user is making fast movement  it's important nevertheless to keep track of a color detected. Then the user can come back slower to explore it.
 * 
 */
void Mit5::Device::on_matrix_updated()
{
    {
        std::scoped_lock lock(this->matrixMut);
        bool valid = true;
        std::vector<bool>  mat = proxy->Matrix_get(&valid);
        
        for (auto i = 0; i < 16; i++)
        {
            matrix |= mat[i] << Device::translation_table[i];
        }
         dbus_updated = true;
    }
     this->dbus_write.notify_one();
}


/**
 * @brief start 3 threads
 * 
 */
void Mit5::Device::start()
{
    std::thread write_thread(std::bind(&Device::write_thread, this));
    std::thread read_thread(std::bind(&Device::read_thread, this));
    
    dbus_thread();
}
