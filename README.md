# TACTOS-MIT5

Tactos-MIT5 is the driver to connect tactos-dbus with a MIT5 module. MIT5 is a braille device with only 2 cells connected to the computer by serial Port. This module has been developped by the [University of Technology of Compiegne (UTC)](https://www.utc.fr/)

This  software is designed to work with [tactos-core-color](https://gitlab.com/tactos/tactos-core-color)  software. 

## Building

### Dependecies

#### Build dependency
 - g++
 - meson
 - libx11-dev
 - libxtst-dev
 - pkg-config
 - libglibmm-2.4-dev (tactos-dbus dependency)

## Compilation

 Tactos-MIT5 uses [meson](https://mesonbuild.com/) as build system.

```bash
meson --buildtype=release builddir 
cd builddir
ninja
```
executable is called `mit5`

## Installation

```bash
cd buildir
ninja Install
```


### Documentation

A doxygen documentation can be found [here](https://tactos.gitlab.io/mit5-driver/)

## Contact

This project is developed by the the laboratory of [Costech](http://www.costech.utc.fr/) in the [University of Technology of Compiegne (UTC)](https://www.utc.fr/) in partnership with [hypra](http://hypra.fr/-Accueil-1-.html) and [natbraille](http://natbraille.free.fr/).
