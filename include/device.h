#pragma once
#include <array>
#include <mutex>
#include<string>
#include<condition_variable>
#include <libserial/SerialPort.h>
#include "tactosIPC_proxy.h"

#include <X11/Xlib.h>
#include <X11/keysym.h>



namespace Mit5 {
    
    constexpr std::array<uint16_t, 256> crc16_tab_compute()
    {
        std::array<uint16_t, 256> result = {0};
        
        for (size_t i=0; i<256; i++) {
            
            uint16_t crc = 0;
            uint16_t c   = i;
            
            for (size_t j=0; j<8; j++) {
                
                if ( (crc ^ c) & 0x0001 ) crc = ( crc >> 1 ) ^ 0xA001;
                else                      crc =   crc >> 1;
                
                c = c >> 1;
            }
            
            result[i] = crc;
        }
        return result;
    }
    
    class Device {
    public:
        Device(std::pair<uint8_t, uint8_t> button_keycodes,  std::string address = "/dev/tactos", 
               LibSerial::BaudRate baud_rate = LibSerial::BaudRate::BAUD_115200,
               LibSerial::CharacterSize char_size = LibSerial::CharacterSize::CHAR_SIZE_8,
               LibSerial::FlowControl flow = LibSerial::FlowControl::FLOW_CONTROL_DEFAULT,
               LibSerial::Parity parity = LibSerial::Parity::PARITY_NONE,
               LibSerial::StopBits stopbits = LibSerial::StopBits::STOP_BITS_1);
        void start();
    
    
    private:
        std::pair<uint8_t, uint8_t> crc16(const std::array<unsigned char, 4> trame );
        std::pair<bool, bool> get_buttons_status();
        void on_matrix_updated();
        void write_trame(uint8_t cell1, uint8_t cell2);
        void read_thread();
        void write_thread();
        void dbus_thread();
        
        std::mutex serialPortMut;
        std::mutex matrixMut;
        std::condition_variable dbus_write;
        bool dbus_updated;
        
        Glib::RefPtr<org::tactos::ipcProxy> proxy;
        LibSerial::SerialPort port;
        uint16_t matrix;
        uint8_t button_1;
        uint8_t button_2;
        
        // X11 connection to send fake keypress event
        Display* display;
        KeyCode button_1_code;
        KeyCode button_2_code;
        
        static constexpr std::array<uint16_t, 256> crc_tab16 = crc16_tab_compute();
        static constexpr std::array<uint8_t, 16> translation_table = {
            0x7, 0x3, 0xf, 0xb,
            0x6, 0x2, 0xe, 0xa,
            0x5, 0x1,  0xd, 0x9,
            0x4, 0x0, 0xc, 0x8};
    };
    
    
    
    
}
